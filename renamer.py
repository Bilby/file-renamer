#!/usr/bin/python

"""
Usage: renamer <path> <season number - as string, put 02 if you want that...>
"""

import sys, os

path = sys.argv[1]
season = sys.argv[2]

dirlist = os.listdir(path)
dirlist.sort()

digits = len(str(len(dirlist)+1))

if digits < 2:
    digits = 2

for i, fname in enumerate(dirlist):
    episode = i+1
    cdigits = len(str(episode))
    zeros = digits - cdigits
    nname = "S%sE" % season

    for z in range(zeros):
        nname += '0'
    
    nname += str(episode)
    
    sname = fname.split('.')

    if len(sname) > 1:
        nname += ".%s" % sname[-1]

    print "renaming: \"%s\" to: \"%s\"" % (fname, nname)

    fname = "%s/%s" % (path, fname)
    nname = "%s/%s" % (path, nname)

    os.rename(fname, nname)

