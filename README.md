# TV show renamer script #

This is a script to simply rename files in a folder to the correct format that Plex (and other self hosted media services) can see.
This tool was created to help when importing Blu Ray seasons of shows to our media server.

The script is in very early stages, and does not currently do very much except rename seasonal folder contents correctly!

## Known issues ##

* Does not check for exceptions properly, if something goes wrong - it goes really wrong!
* Will rename seperate subtitle files as new episodes.

## License ##

This software uses the MIT license.

## Disclaimer ##

This tool is not intended to aide media piracy, please use it on media you actually own!
